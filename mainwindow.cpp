#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QString>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QCryptographicHash>
#include <QFile>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setFixedSize(size());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_mapsPathButton_clicked()
{
     QString dir = QFileDialog::getExistingDirectory(this, tr("Wybierz folder, w której masz MapPacki"),
     "",QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);


     ui->mapsPathLineEdit->setText(dir);
}

void MainWindow::on_jsonPathButton_clicked()
{
    QString dir = QFileDialog::getSaveFileName(this, tr("Wybierz ścieżkę dla pliku który zostanie wygenerowany"), "index.json", "JSON file (*.json);;Any file (*.*)");

    ui->jsonPathLineEdit->setText(dir);
}

void MainWindow::on_generateButton_clicked()
{
    if(ui->jsonPathLineEdit->text() == nullptr)
    {
        QMessageBox::critical(this, this->window()->windowTitle(),
            "Błąd, nie wybrano ścieżki do wygenerowanego pliku.");
        return;
    }

    if(ui->mapsPathLineEdit->text() == nullptr)
    {
        QMessageBox::critical(this, this->window()->windowTitle(),
            "Błąd, nie wybrano ścieżki do map packa.");
        return;
    }

    QDir directory(ui->mapsPathLineEdit->text());

    QJsonDocument document;
    QJsonObject map;
    QJsonArray maps;

    QString prefix = ui->prefixLineEdit->text();
    QStringList files = directory.entryList(QStringList() << "*.w3x" << "*.w3m", QDir::Files);

    foreach(QString filename, files)
    {
       QCryptographicHash crypto(QCryptographicHash::Md5);
       QFile file(ui->mapsPathLineEdit->text()+"/"+filename);
       if (file.open(QFile::ReadOnly))
       {
           crypto.addData(file.readAll());
           QString hash = crypto.result().toHex();
           map["path"] = prefix+"/"+filename;
           map["md5"] = hash;

           maps.append(map);
           file.close();
       }
    }

    document.setArray(maps);

    QFile jsonFile(ui->jsonPathLineEdit->text());
    jsonFile.open(QFile::WriteOnly);

    if(!ui->alllowWhiteSpaceCheckBox->isChecked())
        jsonFile.write(document.toJson(QJsonDocument::Compact));
    else
        jsonFile.write(document.toJson());

    QMessageBox::information(this, this->window()->windowTitle(),"Wygenerowano !");

}
