/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *generateButton;
    QLabel *mapsPathLabel;
    QLineEdit *mapsPathLineEdit;
    QToolButton *mapsPathButton;
    QLabel *jsonPathLabel;
    QLineEdit *jsonPathLineEdit;
    QToolButton *jsonPathButton;
    QCheckBox *alllowWhiteSpaceCheckBox;
    QLabel *prefixLabel;
    QLineEdit *prefixLineEdit;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(453, 186);
        MainWindow->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        generateButton = new QPushButton(centralWidget);
        generateButton->setObjectName(QStringLiteral("generateButton"));
        generateButton->setGeometry(QRect(380, 160, 67, 21));
        generateButton->setAutoDefault(true);
        mapsPathLabel = new QLabel(centralWidget);
        mapsPathLabel->setObjectName(QStringLiteral("mapsPathLabel"));
        mapsPathLabel->setGeometry(QRect(150, 10, 131, 16));
        mapsPathLineEdit = new QLineEdit(centralWidget);
        mapsPathLineEdit->setObjectName(QStringLiteral("mapsPathLineEdit"));
        mapsPathLineEdit->setGeometry(QRect(10, 30, 411, 21));
        mapsPathButton = new QToolButton(centralWidget);
        mapsPathButton->setObjectName(QStringLiteral("mapsPathButton"));
        mapsPathButton->setGeometry(QRect(420, 29, 26, 21));
        jsonPathLabel = new QLabel(centralWidget);
        jsonPathLabel->setObjectName(QStringLiteral("jsonPathLabel"));
        jsonPathLabel->setGeometry(QRect(130, 60, 191, 16));
        jsonPathLineEdit = new QLineEdit(centralWidget);
        jsonPathLineEdit->setObjectName(QStringLiteral("jsonPathLineEdit"));
        jsonPathLineEdit->setGeometry(QRect(10, 80, 411, 21));
        jsonPathButton = new QToolButton(centralWidget);
        jsonPathButton->setObjectName(QStringLiteral("jsonPathButton"));
        jsonPathButton->setGeometry(QRect(420, 79, 26, 21));
        alllowWhiteSpaceCheckBox = new QCheckBox(centralWidget);
        alllowWhiteSpaceCheckBox->setObjectName(QStringLiteral("alllowWhiteSpaceCheckBox"));
        alllowWhiteSpaceCheckBox->setGeometry(QRect(10, 160, 72, 19));
        alllowWhiteSpaceCheckBox->setCheckable(true);
        alllowWhiteSpaceCheckBox->setChecked(true);
        prefixLabel = new QLabel(centralWidget);
        prefixLabel->setObjectName(QStringLiteral("prefixLabel"));
        prefixLabel->setGeometry(QRect(6, 110, 441, 16));
        prefixLabel->setAlignment(Qt::AlignCenter);
        prefixLineEdit = new QLineEdit(centralWidget);
        prefixLineEdit->setObjectName(QStringLiteral("prefixLineEdit"));
        prefixLineEdit->setGeometry(QRect(10, 130, 431, 21));
        MainWindow->setCentralWidget(centralWidget);
        QWidget::setTabOrder(mapsPathLineEdit, mapsPathButton);
        QWidget::setTabOrder(mapsPathButton, jsonPathLineEdit);
        QWidget::setTabOrder(jsonPathLineEdit, jsonPathButton);
        QWidget::setTabOrder(jsonPathButton, prefixLineEdit);
        QWidget::setTabOrder(prefixLineEdit, alllowWhiteSpaceCheckBox);
        QWidget::setTabOrder(alllowWhiteSpaceCheckBox, generateButton);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Generator MapPack\303\263w", 0));
        generateButton->setText(QApplication::translate("MainWindow", "Generuj", 0));
        mapsPathLabel->setText(QApplication::translate("MainWindow", "Podaj \305\233cie\305\274k\304\231 do MapPacka", 0));
        mapsPathButton->setText(QApplication::translate("MainWindow", "...", 0));
        jsonPathLabel->setText(QApplication::translate("MainWindow", "\305\232cie\305\274ka do wygenerowanego pliku .json", 0));
        jsonPathLineEdit->setText(QApplication::translate("MainWindow", "index.json", 0));
        jsonPathButton->setText(QApplication::translate("MainWindow", "...", 0));
        alllowWhiteSpaceCheckBox->setText(QApplication::translate("MainWindow", "Bia\305\202e znaki", 0));
        prefixLabel->setText(QApplication::translate("MainWindow", "Prefix \305\233cie\305\274ki (opcjonalny)", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
